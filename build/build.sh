#!/bin/bash

# script forked from https://gist.github.com/mshafiee/5a681bbefda8f26f1f257d62f5e4a699

# run in root dir eg. ./build/build.sh <binname> <version>

BIN_FILE_NAME_PREFIX=$1
version=$2
PROJECT_DIR=$PWD
PLATFORMSALL=$(go tool dist list)
PLATFORMSCORE="\
linux/amd64 linux/arm64 linux/386 linux/arm linux/riscv64 \
windows/amd64 windows/arm64 windows/386 windows/arm \
darwin/amd64 darwin/arm64 \
"
PLATFORMSMOBILE="\
android/386 android/amd64 android/arm android/arm64 \
ios/amd64 ios/arm64
"

if [ -z "$BIN_FILE_NAME_PREFIX" ]; then 
echo "Please enter bin name (next time use ./build/build.sh <binname>)"
read BIN_FILE_NAME_PREFIX
fi

if [ -z "$version" ]; then
echo "What version? (no v, just numbers with periods--can also specify via ./build/build.sh <binname> <version>)"
read version
fi

echo "What platforms do you want to compile (a=all*, c=core {windows linux mac}, m=mobile {android ios})"
read platform

if [ "$platform" == "c" ]; then
 PLATFORMS=$PLATFORMSCORE
 echo "You are compiling CORE"
elif [ "$platform" == "m" ]; then
 PLATFORMS=$PLATFORMSMOBILE
 echo "You are compiling MOBILE"
else
 PLATFORMS=$PLATFORMSALL
 echo "You are compiling ALL"
fi

FILEPATH="$PROJECT_DIR/bin"
mkdir -p $FILEPATH
cat /dev/null > $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt

for PLATFORM in $PLATFORMS; do
        GOOS=${PLATFORM%/*}
        GOARCH=${PLATFORM#*/}
        BIN_FILE_NAME="$FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}_${GOOS}_${GOARCH}"
        if [ "${GOOS}" = "windows" ]; then BIN_FILE_NAME="${BIN_FILE_NAME}.exe"; fi
        CMD="CGO_ENABLED=0 GOOS=${GOOS} GOARCH=${GOARCH} go build -ldflags '-w -s' -o ${BIN_FILE_NAME}"
        echo "${CMD}"
        eval $CMD || FAILURES="${FAILURES} ${PLATFORM}"

        if [ -f "${BIN_FILE_NAME}" ]; then
        echo "md5sum `md5sum ${BIN_FILE_NAME}`" >> $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt
        echo "sha1sum `sha1sum ${BIN_FILE_NAME}`" >> $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt
        echo "sha256sum `sha256sum ${BIN_FILE_NAME}`" >> $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt
        echo "sha384sum `sha384sum ${BIN_FILE_NAME}`" >> $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt
        echo "sha512sum `sha512sum ${BIN_FILE_NAME}`" >> $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt
        fi
done

echo "removing filepath from sum"
sed -i 's,'"${FILEPATH}/"',,g' "$FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt"
