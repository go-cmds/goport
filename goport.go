package main

import (
    "fmt"
    "net"
    "os"
    "time"
)

func main() {
    if len(os.Args) != 3 {
        fmt.Println("version is v1.0.0")
        fmt.Fprintf(os.Stderr, "Usage: %s [tcp/udp] host:port\n", os.Args[0])
        os.Exit(1)
    }

    protocol := os.Args[1]
    address := os.Args[2]

    switch protocol {
    case "tcp":
        conn, err := net.DialTimeout("tcp", address, 1*time.Second)
        if err != nil {
            fmt.Println("Error:", err)
            os.Exit(1)
        }
        conn.Close()
        fmt.Printf("TCP port %s is open on %s\n", address, conn.LocalAddr())
    case "udp":
        udpAddr, err := net.ResolveUDPAddr("udp", address)
        if err != nil {
            fmt.Println("Error:", err)
            os.Exit(1)
        }
        conn, err := net.DialUDP("udp", nil, udpAddr)
        if err != nil {
            fmt.Println("Error:", err)
            os.Exit(1)
        }
        conn.Close()
        fmt.Printf("UDP port %s is open on %s\n", address, conn.LocalAddr())
    default:
        fmt.Fprintf(os.Stderr, "Invalid protocol: %s\n", protocol)
        os.Exit(1)
    }
}